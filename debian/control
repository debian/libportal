Source: libportal
Section: libs
Priority: optional
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders:
 Simon McVittie <smcv@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-gir,
 gir1.2-freedesktop-dev,
 gir1.2-gio-2.0-dev,
 gir1.2-gtk-3.0-dev <!pkg.libportal.nogtk3>,
 gir1.2-gtk-4.0-dev <!pkg.libportal.nogtk4>,
 gobject-introspection,
 gobject-introspection (>= 1.78.1-9~) <cross>,
 libglib2.0-dev (>= 2.72),
 libgstreamer-plugins-base1.0-dev <!noinsttest>,
 libgtk-3-dev <!pkg.libportal.nogtk3>,
 libgtk-4-dev <!pkg.libportal.nogtk4>,
 libqt5x11extras5-dev <!pkg.libportal.noqt5>,
 meson (>= 0.49.0),
 meson (>= 0.63.0) <!pkg.libportal.noqt6>,
 pkgconf,
 python3-dbus <!nocheck>,
 python3-dbusmock <!nocheck>,
 python3-pytest <!nocheck>,
 qtbase5-dev <!pkg.libportal.noqt5>,
 qt6-base-dev <!pkg.libportal.noqt6>,
 qt6-base-private-dev <!pkg.libportal.noqt6>,
 valac:native,
 xauth <!nocheck>,
 xvfb <!nocheck>,
Build-Depends-Indep:
 gi-docgen <!nodoc>,
 gjs <!noinsttest>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/flatpak/libportal
Vcs-Git: https://salsa.debian.org/debian/libportal.git
Vcs-Browser: https://salsa.debian.org/debian/libportal

Package: gir1.2-xdp-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 ${gir:Provides},
Description: Flatpak portal library - introspection data
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package can be used by other packages using the GIRepository format
 to generate dynamic bindings.

Package: gir1.2-xdpgtk3-1.0
Build-Profiles: <!pkg.libportal.nogtk3>
Section: introspection
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 ${gir:Provides},
Description: Flatpak portal library - introspection data for GTK 3
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package can be used by other packages using the GIRepository format
 to generate dynamic bindings for GTK 3 integration.

Package: gir1.2-xdpgtk4-1.0
Build-Profiles: <!pkg.libportal.nogtk4>
Section: introspection
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 ${gir:Provides},
Description: Flatpak portal library - introspection data for GTK 4
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package can be used by other packages using the GIRepository format
 to generate dynamic bindings for GTK 4 integration.

Package: libportal-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gir1.2-xdp-1.0 (= ${binary:Version}),
 libglib2.0-dev (>= 2.72),
 libportal1 (= ${binary:Version}),
 pkgconf,
 ${gir:Depends},
 ${misc:Depends},
Provides:
 ${gir:Provides},
Description: Flatpak portal library (development files)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains development headers and the pkg-config file for
 the non-GUI-specific parts of libportal.

Package: libportal-doc
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
Description: Flatpak portal library (documentation)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains API documentation.

Package: libportal-gtk3-1
Build-Profiles: <!pkg.libportal.nogtk3>
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Flatpak portal library for GTK 3 GUIs
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the shared library for integration with GTK 3.

Package: libportal-gtk3-dev
Build-Profiles: <!pkg.libportal.nogtk3>
Architecture: any
Multi-Arch: same
Section: libdevel
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gir1.2-xdpgtk3-1.0 (= ${binary:Version}),
 libglib2.0-dev (>= 2.72),
 libgtk-3-dev,
 libportal-dev (= ${binary:Version}),
 libportal-gtk3-1 (= ${binary:Version}),
 pkgconf,
 ${gir:Depends},
 ${misc:Depends},
Provides:
 ${gir:Provides},
Description: Flatpak portal library (development files for GTK 3)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains development headers and the pkg-config file for
 integration with GTK 3.

Package: libportal-gtk4-1
Build-Profiles: <!pkg.libportal.nogtk4>
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Flatpak portal library for GTK 4 GUIs
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the shared library for integration with GTK 4.

Package: libportal-gtk4-dev
Build-Profiles: <!pkg.libportal.nogtk4>
Architecture: any
Multi-Arch: same
Section: libdevel
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gir1.2-xdpgtk4-1.0 (= ${binary:Version}),
 libglib2.0-dev (>= 2.72),
 libgtk-4-dev,
 libportal-dev (= ${binary:Version}),
 libportal-gtk4-1 (= ${binary:Version}),
 pkgconf,
 ${gir:Depends},
 ${misc:Depends},
Provides:
 ${gir:Provides},
Description: Flatpak portal library (development files for GTK 4)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains development headers and the pkg-config file for
 integration with GTK 4.

Package: libportal-qt5-1
Build-Profiles: <!pkg.libportal.noqt5>
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Flatpak portal library for Qt 5 GUIs
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the shared library for integration with Qt 5.

Package: libportal-qt5-dev
Build-Profiles: <!pkg.libportal.noqt5>
Architecture: any
Multi-Arch: same
Section: libdevel
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 libportal-dev (= ${binary:Version}),
 libportal-qt5-1 (= ${binary:Version}),
 libqt5x11extras5-dev,
 pkgconf,
 qtbase5-dev,
 ${misc:Depends},
Description: Flatpak portal library (development files for Qt 5)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains development headers and the pkg-config file for
 integration with Qt 5.

Package: libportal-tests-gtk3
Build-Profiles: <!noinsttest !pkg.libportal.nogtk3>
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 gnome-backgrounds,
Description: Flatpak portal library (test program for GTK 3)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the portal-test-gtk3 application, and will contain
 "as-installed" test programs if they are added.

Package: libportal-tests-gtk4
Build-Profiles: <!noinsttest !pkg.libportal.nogtk4>
Architecture: all
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 gir1.2-gdkpixbuf-2.0,
 gir1.2-gstreamer-1.0,
 gir1.2-gtk-4.0,
 gir1.2-pango-1.0,
 gir1.2-xdp-1.0,
 gir1.2-xdpgtk4-1.0,
 gjs,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 gnome-backgrounds,
Description: Flatpak portal library (test program for GTK 4)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the org.gnome.PortalTest.Gtk4 application, and
 will contain "as-installed" test programs if they are added.

Package: libportal-tests-qt5
Build-Profiles: <!noinsttest !pkg.libportal.noqt5>
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Flatpak portal library (test program for Qt)
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the portal-test-qt application, and will contain
 "as-installed" test programs if they are added.

Package: libportal1
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Flatpak portal library - non-GUI part
 libportal provides GIO-style C APIs for most Flatpak portals' D-Bus
 interfaces. It is primarily intended for installation in Flatpak runtimes,
 where applications can use it to interact with portals; it can also be
 used to test the portal services.
 .
 See the xdg-desktop-portal package's description for more information
 about portals.
 .
 This package contains the shared library for non-GUI-specific use of
 portals.
